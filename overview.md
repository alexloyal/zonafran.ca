## Name: zonafran.ca



## Platform: Web Application
- Content Management System
- Web -based
- Mobile friendly


## Content: Thought leadership aimed at 0-generation hispanic, age  18-40

They want to stay in the US, and want to better understand what it's like to live here.

- Current Affairs
	Distill topics for 
- Data journalism
- Focus on latin american current affairs

https://prodavinci.com/
http://lasillavacia.com/
https://hbr.org/

- License content

- English based (con opcion en español)

No finite number of eyeballs

- Whatsapp

- Economic Development
	- Public sector and private sector startups
	- Ciudades como hubs, lugares / cuna de desarrollo de industrias en especific
	- Fomentar desarrollo, ciudades emergentes y sostenible iadb.org
- Financial advisory content
- Business advice for young entrepreneurs, interview emerging latin american young business leaders
- Interview Social media influencers

## Strategy

- Use Twitter API to create a dashboard of trending topics by country, and have a way to drill down by topic. 
- Build it into the CMS.


## Market Research
- Surveys, feasibility of content
- Contact lists
 	https://endeavor.org/


## Business Model
- 5 free articles a month
	Cookie based access restriction

- Free 30-day trial to access all content
	(Need: user research, and how do we reach them)

- Subscription to access all content, including premium features like
	- Newsletters
	- Podcasts
	- Video
	- Long-form reporting

- Advertisment
- Partnerships
- Speaker Series event about current events, economic development, finance.
- Paid content
- Seek grants on reporting and fomenting economic development for improving society, working with local governments that get grants from banks.


## Marketing
- Search engine marketing via paid ads
- Social media marketing via paid ads

## Design and User Experience

Impecable design and user experience. 
Photography licensing from getty for editorial images
https://www.gettyimages.com/plans-and-pricing

##CMS
KeystoneJS
	- jade template engine
	- 


